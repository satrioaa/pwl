<!DOCTYPE html>
<html>
<head>
    <title>Kalkulator PHP</title>
    <style>
        body{
            font-family: cursive;
            background: #E5E4E2;
        }
        h1{
            text-align: center;
            /*ketebalan font*/
            font-weight: 300;
        }
        table{
            width: 35%;
            /*meletakkan form ke tengah*/
            margin: 30px auto;
            border-collapse: collapse;
        }
        /*membuat garis pada tabel*/
        table, th, td{
            border: 1px solid #3c3c3c;
            padding: 3px 8px;
            text-align: center;
        }
        /*membuat form menjadi lebih rapi*/
        input[type="number"]{
            padding: 10px;
            width: 93%;
            border: 0;
            border-radius: 3px;
        }
        input[type="submit"]{
            padding: 10px 20px;
            background: #3c3c3c;
            color: #fff;
            border: 0;
            border-radius: 3px;
        }
        
    </style>
<body>
    <h1>Kalkulator PHP</h1>
    <form action="" method="post">
        <table>
            <tr>
                <td width="200">Angka 1</td>
                <td>:</td>
                <td><input type="number" name="input1"></td>
            </tr>
            <tr>
                <td>Angka 2</td>
                <td>:</td>
                <td><input type="number" name="input2"></td>
            </tr>
            <tr>
                <td>Operator</td>
                <td>:</td>
                <td>
                    <select name="operator">
                        <option value="tambah">pertambahan</option>
                        <option value="kurang">pengurangan</option>
                        <option value="kali">perkalian</option>
                        <option value="bagi">pembagian</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" name="hitung" value="Hitung"></td>
            </tr>
        </table><br><br>

        <?php
            if(isset($_POST['hitung'])){
                $input1 = $_POST['input1'];
                $input2 = $_POST['input2'];
                $operator = $_POST['operator'];
                switch($operator){
                    case 'tambah':
                        $hasil = $input1 + $input2;
                    break;
                    case 'kurang':
                        $hasil = $input1 - $input2;
                    break;
                    case 'kali':
                        $hasil = $input1 * $input2;
                    break;
                    case 'bagi':
                        $hasil = $input1 / $input2;
                    break;
                }
                
                echo "<center>Hasil dari $input1 $operator $input2 adalah $hasil</center>";
            }
            $faktorial = 1;
            for($i = 1; $i <= $hasil; $i++){
                $faktorial = $faktorial * $i;
            }
            echo "<center>Hasil dari $hasil! adalah $faktorial</center>";
        ?>
</body>
</html>